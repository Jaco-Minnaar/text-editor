use std::{fs, io::Error};

use crate::{Position, Row};

#[derive(Default)]
pub struct Document {
    rows: Vec<Row>,
    pub file_name: Option<String>,
}

impl Document {
    pub fn open(filename: &str) -> Result<Self, Error> {
        let contents = fs::read_to_string(filename)?;
        let rows = contents.lines().map(Row::from).collect();

        Ok(Document {
            rows,
            file_name: Some(filename.to_string()),
        })
    }

    pub fn row(&self, index: usize) -> Option<&Row> {
        self.rows.get(index)
    }

    pub fn is_empty(&self) -> bool {
        self.rows.is_empty()
    }

    pub fn len(&self) -> usize {
        self.rows.len()
    }

    fn insert_newline(&mut self, pos: &Position) {
        if pos.y > self.len() {
            return;
        }

        if pos.y == self.len() {
            self.rows.push(Row::default());

            return;
        }

        let new_row = self.rows.get_mut(pos.y).unwrap().split(pos.x);
        self.rows.insert(pos.y + 1, new_row);
    }

    pub fn insert(&mut self, pos: &Position, c: char) {
        if c == '\n' {
            self.insert_newline(pos);
            return;
        }

        if pos.y == self.len() {
            let mut row = Row::default();
            row.insert(0, c);
            self.rows.push(row);
        } else if pos.y < self.len() {
            let row = self.rows.get_mut(pos.y).unwrap();
            row.insert(pos.x, c);
        }
    }

    pub fn delete(&mut self, pos: &Position) {
        let len = self.len();

        if pos.y > self.len() {
            return;
        }

        if pos.x == self.rows.get_mut(pos.y).unwrap().len() && pos.y < len - 1 {
            let next_row = self.rows.remove(pos.y + 1);
            let row = self.rows.get_mut(pos.y).unwrap();
            row.append(&next_row);

            return;
        }

        let row = self.rows.get_mut(pos.y).unwrap();
        row.delete(pos.x);
    }
}
