use std::cmp;

use unicode_segmentation::UnicodeSegmentation;

#[derive(Default)]
pub struct Row {
    string: String,
    len: usize,
}

impl From<&str> for Row {
    fn from(from_string: &str) -> Self {
        let mut row = Self {
            string: from_string.to_string(),
            len: 0,
        };

        row.update_len();
        row
    }
}

impl Row {
    pub fn render(&self, start: usize, end: usize) -> String {
        let end = cmp::min(end, self.string.len());
        let start = cmp::min(start, end);
        // self.string.get(start..end).unwrap_or_default().to_string()

        let mut result = String::new();
        for grapheme in self.string[..]
            .graphemes(true)
            .skip(start)
            .take(end - start)
        {
            if grapheme == "\t" {
                result.push_str("    ");
            } else {
                result.push_str(grapheme);
            }
        }

        result
    }

    pub fn len(&self) -> usize {
        self.len
    }

    pub fn is_empty(&self) -> bool {
        self.len == 0
    }

    fn update_len(&mut self) {
        self.len = self.string[..].graphemes(true).count();
    }

    pub fn insert(&mut self, pos: usize, c: char) {
        if pos >= self.len() {
            self.string.push(c);
        } else {
            let mut result: String = self.string[..].graphemes(true).take(pos).collect();
            let remainder: String = self.string[..].graphemes(true).skip(pos).collect();
            result.push(c);
            result.push_str(&remainder);
            self.string = result;
        }

        self.update_len();
    }

    pub fn delete(&mut self, pos: usize) {
        if pos >= self.len() {
            return;
        } else {
            let mut result: String = self.string[..].graphemes(true).take(pos).collect();
            let remainder: String = self.string[..].graphemes(true).skip(pos + 1).collect();
            result.push_str(&remainder);
            self.string = result;
        }

        self.update_len();
    }

    pub fn append(&mut self, new: &Self) {
        self.string = format!("{}{}", self.string, new.string);
        self.update_len();
    }

    pub fn split(&mut self, pos: usize) -> Self {
        let beginning: String = self.string[..].graphemes(true).take(pos).collect();
        let remainder: String = self.string[..].graphemes(true).skip(pos).collect();
        self.string = beginning;
        self.update_len();
        Self::from(&remainder[..])
    }
}
